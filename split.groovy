// Copyright (C) 2013 Rudy Neeser
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.


/**
 * This takes in a file and splits it in to multiple files. The splitting occurs whenever multiple blank lines
 * are followed by a single line in all caps.
 */


// Check that we got a file name, and that we can open it.
String name = args ? args[0] : null

if (!name) {
    System.err.println("No file name was given")
    System.exit(1)
}

File inputFile = new File(name)
if (!inputFile.exists()) {
    System.err.println("${name} does not exist")
    System.exit(1)
}

FileReader reader = new FileReader(inputFile)
StringBuilder builder = new StringBuilder()
String newFileName = null

boolean first = true
int emptyLine = 0
int sectionNumber = 1

def createFilename = { line ->
    "${(sectionNumber++).toString().padLeft(3, '0')}-${line.replaceAll(/\W+/, '-')}.txt"
}

reader.eachLine { String line ->
    line = line.trim()

    // Empty lines mean we might be finding a heading.
    if (!line) {
        emptyLine++
        builder.append("\n")
        return
    }

    // We need to determine the title of the first story
    if (first) {
        first = false
        newFileName = createFilename(line)
    }

    // A heading is two empty lines followed by an all caps line.
    if (emptyLine >= 2) {
        boolean isHeading = line.every {
            char c = it.charAt(0)
            return c.isUpperCase() || c.isWhitespace()
        }

        if (isHeading) {
            // Write out previous file, if there is one.
            if (newFileName) {
                new File(newFileName).withPrintWriter { writer ->
                    writer.println(builder.toString())
                }
            }

            newFileName = createFilename(line)
            builder = new StringBuilder()
        }
    }

    emptyLine = 0
    builder.append(line)
    builder.append("\n")
}

if (newFileName) {
    new File(newFileName).withPrintWriter { writer ->
        writer.println(builder.toString())
    }
}
