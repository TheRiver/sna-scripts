// Copyright (C) 2013 Rudy Neeser
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

import tf_idf4j.api.Corpus
import tf_idf4j.api.Document
// Javatools is available from http://www.mpi-inf.mpg.de/yago-naga/javatools/
// The Javatools are licensed under a Creative Commons Attribution 3.0 License (http://creativecommons.org/licenses/by/3.0/)
// by the YAGO-NAGA team (http://www.mpi-inf.mpg.de/yago-naga/).
import javatools.parsers.PlingStemmer
import tf_idf4j.api.LogFrequencyStrategy
import tf_idf4j.api.StandardTokeniser
import tf_idf4j.api.Tokeniser


class PluralTokeniser implements Tokeniser {

    private Tokeniser tok = new StandardTokeniser()

    @Override
    String[] split(String s) {
        String[] words = tok.split(s)

        for (int i = 0; i < words.length; i++) {
            if (words[i] in ['hans']) continue;
            words[i] = PlingStemmer.stem(words[i])
        }

        words
    }
}


println ("Hello world")

def grim = new File("/home/rudy/Development/sna/working/source-documents/grimm/stories")
def aesop = new File("/home/rudy/Development/sna/working/source-documents/aesop/stories")

Corpus corpus = new Corpus(new PluralTokeniser(), new LogFrequencyStrategy())

grim.eachFile { File file -> corpus.addDocument("gr-${file.name}", file) }
aesop.eachFile { File file -> corpus.addDocument("ae-${file.name}", file) }



println "There are ${corpus.numberOfWords} unique words in this corpus"
println "There are ${corpus.documents.size()} stories in this corpus"
println "There are a total of ${corpus.words.sum { corpus.getFrequency(it) }} words in total"

println ""
println "The word [the] is contained in ${corpus.getNumberOfDocumentsWithWord('the')} documents"
println "The word [black] is contained in ${corpus.getNumberOfDocumentsWithWord('black')} documents"
println "The word [white] is contained in ${corpus.getNumberOfDocumentsWithWord('white')} documents"
println "The word [red] is contained in ${corpus.getNumberOfDocumentsWithWord('red')} documents"
println "The word [bat] is contained in ${corpus.getNumberOfDocumentsWithWord('bat')} documents"

Document hAndG = corpus.documents.find{ it.id == "gr-15-HANSEL-AND-GRETEL.txt" }
println ""
println "The tf-idf of [the] in Hansel and Gretel is [${corpus.getTfIdf("the", hAndG)}]"
println "The tf-idf of [hansel] in Hansel and Gretel is [${corpus.getTfIdf("hansel", hAndG)}]"
println "The tf-idf of [gretel] in Hansel and Gretel is [${corpus.getTfIdf("gretel", hAndG)}]"
println "The ${hAndG.getFrequency('the')}, hansel ${hAndG.getFrequency('hansel')}, gretel ${hAndG.getFrequency('gretel')}"




// Find the most important words in each story
Map<String,Integer> counts = [:]
// The list of tf-idfs for a word, one measurement per document
Map<String, List<Double>> wordToTfIdfs = [:]
final int wordsToTake = 10
Map<Document, List<String>> documentToImportantWords = [:]

corpus.documents.each { Document document ->
    def words = document.words.toList().sort(true) { lhs, rhs ->
        corpus.getTfIdf(rhs, document) <=> corpus.getTfIdf(lhs, document)
    }

    documentToImportantWords[document] = words.take(wordsToTake)
    documentToImportantWords[document].each { String word ->
        double value = corpus.getTfIdf(word, document)
        wordToTfIdfs.get(word, []) << value
        counts[word] = 1 + counts.get(word, 0)
    }
}

def sharedWords = counts.keySet().findAll { String word ->
    counts[word] > 1
}

println "There are ${sharedWords.size()} words shared more than once."
println sharedWords.sort()

class Node {
    String word
    int frequency
    int documents
    List<Double> tfIdfs

    @Override
    String toString() {
        "${word} × ${frequency}"
    }

    String toGml(int i) {
        Double max = tfIdfs?.max()
        Double min = tfIdfs?.min()
        Double ave = tfIdfs ? ((double)tfIdfs.sum() / (double)tfIdfs.size()) : null

        "\tnode [\n" +
                "\t\tid ${i}\n" +
                "\t\tlabel ${word}\n" +
                "\t\tfrequency ${frequency}\n" +
                "\t\tdocuments ${documents}\n" +
                (tfIdfs ?  "\t\taveTfIdf ${ave}\n" +
                            "\t\tminTfIdf ${min}\n" +
                            "\t\tmaxTfIdf ${max}\n"
                : "") +
        "\t]\n"
    }
}

class Edge implements Comparable<Edge> {
    String from
    String to
    int weight

    @Override
    boolean equals(Object obj) {
        if (obj instanceof Edge) {
            if (from == obj.from && to == obj.to) return true;
            if (from == obj.to && to == obj.from) return true;
        }

        return false;
    }

    @Override
    int compareTo(Edge o) {
        [from, to].sort().join('') <=> [o.from, o.to].sort().join('')
    }

    @Override
    String toString() {
        "${from} → ${to}"
    }

    String toGml(Map<String, Integer> wordIndices) {
        "\tedge [\n" +
                "\t\tsource ${wordIndices[from]}\n" +
                "\t\ttarget ${wordIndices[to]}\n" +
                (weight ? "\t\tweight ${weight}" : "") +
        "\t]"
    }
}

Set<Node> nodes = []
Set<Edge> edges = new TreeSet<>()
Map<String, List<Document>> wordToDocument = [:]
Map<Document, List<String>> documentToWord = [:]

// For each word, we want to know what documents contain it,
// and we want to keep a record of the words for each document.
counts.keySet().each { String word ->
    corpus.documents.each { Document d ->
        if (d.contains(word)) {
            wordToDocument.get(word, []) << d
            documentToWord.get(d, []) << word
        }
    }
}

// For each word,
// 1. find all the stories that its in
// 2. find the words from those stories
// 3. create an edge

counts.keySet().each { String word ->

    nodes << new Node(
            word: word,
            frequency: corpus.getFrequency(word),
            documents: counts[word],
            tfIdfs: wordToTfIdfs[word]
    )

    List<Document> documents = wordToDocument[word]
    documents.each {
        List<String> linkedTo = documentToWord[it]

        linkedTo.each { String toWord ->
            if (word == toWord) return
            edges << new Edge(from: word, to: toWord)
        }
    }
}

int longestWord = nodes.collect { it.word.length() }.max()

println "\n\nTHE NODES"
List<Node> sortedNodes = nodes.sort{ lhs, rhs -> lhs.documents == rhs.documents ? rhs.frequency <=> lhs.frequency : rhs.documents <=> lhs.documents }.each {
    println "${it.word.padRight(longestWord + 2)}: freqs: ${it.frequency.toString().padRight(3)}, docs: ${it.documents.toString().padRight(3)}"
}

println "\n\nTHE EDGES"
edges.each {
    println it
}

List<Node> documentNodes = []
Map<String, Integer> documentToIndex = [:]
corpus.documents.eachWithIndex { Document d, int i ->
    documentNodes << new Node(
            word: d.id
    )
    documentToIndex[d.id] = i + 1;
}

Set<Edge> documentEdges = []
corpus.documents.each { Document d ->
    Collection<String> importantWords = documentToImportantWords[d]

    println("Working on ${d.id}")
    importantWords.each { String word ->
        corpus.documents.findAll { it.contains(word) }.each {
            documentEdges << new Edge(from: d.id, to: it.id, weight: importantWords.intersect(documentToImportantWords[it]).size())
        }
    }
}

// Save the word network
new File("/tmp/words.gml").withPrintWriter { PrintWriter tales ->
    tales.println("graph [")
    tales.println("\tcomment \"Created by Rudy Neeser for SNA course\"")
    tales.println("\tcomment \"Shows the relationship between words in Grimm's Fairy Tales and Aesop's Fables\"")

    Map<String, Integer> wordToIndex = [:]
    sortedNodes.eachWithIndex { Node node, int i ->
        i += 1
        wordToIndex[node.word] = i

        tales.println(node.toGml(i))
    }

    edges.each { Edge edge ->
        tales.println(edge.toGml(wordToIndex))
    }

    tales.println("]")
}

// Save the story network
new File("/tmp/tales.gml").withPrintWriter { PrintWriter tales ->
    tales.println("graph [")
    tales.println("\tcomment \"Created by Rudy Neeser for SNA course\"")
    tales.println("\tcomment \"Shows the relationship between stories in Grimm's Fairy Tales and Aesop's Fables\"")

    documentNodes.eachWithIndex { Node node, int i ->
        tales.println(node.toGml(i))
    }

    documentEdges.each { Edge edge ->
        tales.println(edge.toGml(documentToIndex))
    }

    tales.println("]")
}
